from django.contrib import admin
from django.urls import path
from client import views,newviews,frontend
from rest_framework.authtoken.views import obtain_auth_token


urlpatterns = [
    path('admin/', admin.site.urls),
    path('addcustomer',views.AddCustomer,name='add'),
    path('allcustomers',views.AllCustomers,name='allcustomers'),
    path('login',views.CustomerLogin,name='login'),
    path('updatecustomer<int:userid>',views.UpdateCustomer,name='update'),
    path('home',views.home,name='home'),
    path('order',views.CreateOrder,name='order'),
    path('showorders',views.orderdetail,name='orders'),
    path('getuser<int:userid>',views.GetUser,name='getuser'),
    path('date',views.datefilter,name='date'),

    # newviews urls
    path('register',newviews.register,name='register'),
    path('userlogin',newviews.userlogin,name='userlogin'),
    path('driver',newviews.drivers,name='driver'),
    path('adduser',newviews.AddUser,name='adduser'),
    path('logout',newviews.userlogout,name='logout'),
    path('dsearch',newviews.driversearch,name='dsearch'),
    path('usearch',newviews.usersearch,name='usearch'),


    # frontend urls
    path('hf',frontend.hf,name='hf'),
    path('about',frontend.about,name='about'),
    path('reveal',frontend.reveal,name='reveal'),
    path('blog',frontend.blog,name='blog'),
    path('contact',frontend.contact,name='contact'),
    path('checkout',frontend.checkout,name='checkout'),
    path('delivery',frontend.delivery,name='delivery'),
    path('login',frontend.login,name='login'),
    path('payment',frontend.payment,name='payment'),
    path('reciept',frontend.reciept,name='reciept'),
    path('product',frontend.product,name='product'),
    path('pgrid',frontend.pgrid,name='pgrid'),
    path('plist',frontend.plist,name='plist'),
    path('ptopbar',frontend.ptopbar,name='ptopbar'),
    path('cprofile',frontend.cprofile,name='cprofile'),
    path('cpnotifications',frontend.cpnotifications,name='cpnotifications'),
    path('cporders',frontend.cporders,name='cporders'),
    path('cprepass',frontend.cprepass,name='cprepass'),
    path('cpwishlist',frontend.cpwishlist,name='cpwishlist'),
    path('services',frontend.services,name='services'),


]