from django.shortcuts import render

# Create your views here.
def hf(request):
    return render(request,"reveal/hf.html")
def reveal(request):
    return render(request,"reveal/reveal.html")
def about(request):
    return render(request,"reveal/about.html")
def blog(request):
    return render(request,"reveal/blog.html")
def contact(request):
    return render(request,"reveal/contact.html")
def checkout(request):
    return render(request,"reveal/checkout.html")
def delivery(request):
    return render(request,"reveal/checkout-delivery.html")
def login(request):
    return render(request,"reveal/checkout-login.html")
def payment(request):
    return render(request,"reveal/checkout-payment.html")
def reciept(request):
    return render(request,"reveal/checkout-reciept.html")
def product(request):
    return render(request,"reveal/productpage.html")
def pgrid(request):
    return render(request,"reveal/products-grid.html")
def plist(request):
    return render(request,"reveal/products-list.html")
def ptopbar(request):
    return render(request,"reveal/products-topbar.html")
def cprofile(request):
    return render(request,"reveal/profile.html")
def cpnotifications(request):
    return render(request,"reveal/prfile-notifications.html")
def cporders(request):
    return render(request,"reveal/prfile-orders.html")
def cprepass(request):
    return render(request,"reveal/prfile-reset-password.html")
def cpwishlist(request):
    return render(request,"reveal/prfile-wishlist.html")
def services(request):
    return render(request,"reveal/services.html")



