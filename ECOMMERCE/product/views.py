from django.shortcuts import render,redirect
from .models import productmodel,category,subcategory
from vendor.models import vendor
from .forms import productform,categoryform
# Create your views here.


def formaddproduct(request):
    
    # form = productform()
    if request.method=='POST':
        
        name = request.POST.get('product_name')
        pdesc = request.POST.get('product_description')
        price = request.POST.get('price')
        stock = request.POST.get('stock')
        categ = request.POST.get('category_name')
        c = category.objects.get(category_name=categ)
        subcat = request.POST.get('subcat_name')
        sc = subcategory.objects.get(subcat_name=subcat)
        stockst = request.POST.get('stock_status')
        vendn = request.POST.get('vendor_email')
        vn = vendor.objects.get(vendor_email=vendn)
        productmodel.objects.create(product_name=name, product_description=pdesc, price=price, stock=stock, category_name=c, subcategory_name=sc,stock_status=stockst,vendor_email=vn)
        
    pl = productmodel.objects.all()
    cl = category.objects.all()
    scl = subcategory.objects.all()
    vl = vendor.objects.all()
    return render(request, "product.html", {'pro':pl, 'cat':cl, 'subc':scl, 'vend':vl} )

def viewproduct(request):
    lp = productmodel.objects.all()
    return render(request, "product.html", {'lp':lp})


def updateproduct(request, productid):
    k = productmodel.objects.filter(id = productid).values()
    if request.method=="POST":
        name = request.POST.get('product_name')
        pdesc = request.POST.get('product_description')
        price = request.POST.get('price')
        stock = request.POST.get('stock')
        categ = request.POST.get('category_name')
        c = category.objects.get(id=categ)
        subcat = request.POST.get('subcat_name')
        sc = subcategory.objects.get(id=subcat)
        stockst = request.POST.get('stock_status')
        vendn = request.POST.get('vendor_email')
        vn = vendor.objects.get(id=vendn) 

        k.update(product_name=name, product_description=pdesc, price=price, stock=stock, category_name=c, subcategory_name=sc,stock_status=stockst,vendor_email=vn)
        
        return redirect('viewproduct')
    
    ls = productmodel.objects.all()
    cl = category.objects.all()
    scl = subcategory.objects.all()
    vl = vendor.objects.all()
    return render(request,"updatedata.html",{"k":k[0],'pro':ls,"id":productid, 'cat':cl, 'subc':scl, 'vend':vl})


def deleteproduct(request,productid):
    dp = productmodel.objects.get(id = productid)
    dp.delete()
    return redirect('viewproduct') 

def addcategory(request):
    if request.method=='POST':
        name = request.POST.get('categ_name')
        desc = request.POST.get('categ_desc')
        vend = request.POST.get('vendor_email')
        vn = vendor.objects.get(vendor_email=vend)

        category.objects.create(category_name=name, category_description=desc, vendor_email=vn)
    vl = vendor.objects.all()
    return render(request, 'category.html', {'vendor':vl}) 

def viewcategory(request):
    lc = category.objects.all()
    return render(request, "viewcategory.html", {'lc':lc})

def deletecategory(request,categoryid):
    cat = category.objects.get(id = categoryid)
    cat.delete()
    return redirect('viewcategory') 

def addsubcategory(request):
    if request.method=='POST':
        name = request.POST.get('subcateg_name')
        desc = request.POST.get('subcateg_desc')
        categ = request.POST.get('category_name')
        c = category.objects.get(category_name=categ)
        vend = request.POST.get('vendor_email')
        vn = vendor.objects.get(vendor_email=vend)

        category.objects.create(subcategory_name=name, subcategory_description=desc, category_name=c, vendor_email=vn)
    cat = category.objects.all()
    vl = vendor.objects.all()
    return render(request, 'subcategory.html',{'vend':vl,'cat':cat})

def viewsubcategory(request):
    lsc = subcategory.objects.all()
    return render(request, "viewsubcategory.html", {'lsc':lsc})

def deletesubcategory(request,subcategoryid):
    subcat = subcategory.objects.get(id = subcategoryid)
    subcat.delete()
    return redirect('viewsubcategory') 